#include "yaml-cpp/yaml.h"

class Product;
struct order_t;

struct Coords_t
{
    double_t cx = -1;
    double_t cy = -1;
};

struct part_t
{
    std::string name;
    size_t linkedProductID = 0;
    Coords_t Coords;
};

class Part
{
    public:
        Part(part_t);
        part_t getPartInfo();
        double getRelDistance(Coords_t);

    private:
        part_t _partInfo;
};

struct product_t
{
    size_t id = 0;
    std::string name = "NullProduct";
};

class Product
{
    public:
        Product(product_t);
        product_t getProductInfo();
        std::vector<Part> getParts();
        void setParts(std::vector<Part>);

    private:
        product_t _productInfo;
        std::vector<Part> _parts;

};

class Products
{
    public:
        Products(std::string pathProductList);
        void printProductsList();
        Product getProdbyID(size_t id);

    private:
        std::vector<Product> _ProductsCatalogue;
};

struct order_t
{
    virtual order_t* orderInfo() = 0;
    size_t getID() {return _id;}
    protected:
        size_t _id = 0;
};

struct intorder_t : public order_t
{
    Coords_t putPlace;
    intorder_t* orderInfo() {return this;}
    void setID(size_t id) {_id = id;}
};

struct extorder_t : public order_t
{
    std::string description;
    extorder_t* orderInfo() {return this;}
    void setID(uint32_t id) {_id = static_cast<size_t>(id);}
};

/*
struct queorder_t : public order_t
{
    std::string storageFile;
    queorder_t* orderInfo() {return this;}
    void setID(size_t id) {_id = id;}
};
*/

class Order
{
    public:
        Order(intorder_t);
        intorder_t* orderInfo();
        std::vector<Product> getProducts();
        void setProducts(std::vector<Product>);

    private:
        intorder_t _orderInfo;
        std::vector<Product> _products;
};

class Orders
{
    public:
        Orders(std::string, Products*);
        void printOrderList();
        Order getOrderbyID(size_t);

    private:
        std::vector<Order> _OrderList;
};