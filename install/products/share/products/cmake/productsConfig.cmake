# generated from ament/cmake/core/templates/nameConfig.cmake.in

# prevent multiple inclusion
if(_products_CONFIG_INCLUDED)
  # ensure to keep the found flag the same
  if(NOT DEFINED products_FOUND)
    # explicitly set it to FALSE, otherwise CMake will set it to TRUE
    set(products_FOUND FALSE)
  elseif(NOT products_FOUND)
    # use separate condition to avoid uninitialized variable warning
    set(products_FOUND FALSE)
  endif()
  return()
endif()
set(_products_CONFIG_INCLUDED TRUE)

# output package information
if(NOT products_FIND_QUIETLY)
  message(STATUS "Found products: 0.0.0 (${products_DIR})")
endif()

# warn when using a deprecated package
if(NOT "" STREQUAL "")
  set(_msg "Package 'products' is deprecated")
  # append custom deprecation text if available
  if(NOT "" STREQUAL "TRUE")
    set(_msg "${_msg} ()")
  endif()
  # optionally quiet the deprecation message
  if(NOT ${products_DEPRECATED_QUIET})
    message(DEPRECATION "${_msg}")
  endif()
endif()

# flag package as ament-based to distinguish it after being find_package()-ed
set(products_FOUND_AMENT_PACKAGE TRUE)

# include all config extra files
set(_extras "ament_cmake_export_targets-extras.cmake")
foreach(_extra ${_extras})
  include("${products_DIR}/${_extra}")
endforeach()
