#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "products::products" for configuration ""
set_property(TARGET products::products APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(products::products PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_NOCONFIG "CXX"
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libproducts.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS products::products )
list(APPEND _IMPORT_CHECK_FILES_FOR_products::products "${_IMPORT_PREFIX}/lib/libproducts.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
