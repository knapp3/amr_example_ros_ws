# generated from ament/cmake/core/templates/nameConfig.cmake.in

# prevent multiple inclusion
if(_orderoptimizer_CONFIG_INCLUDED)
  # ensure to keep the found flag the same
  if(NOT DEFINED orderoptimizer_FOUND)
    # explicitly set it to FALSE, otherwise CMake will set it to TRUE
    set(orderoptimizer_FOUND FALSE)
  elseif(NOT orderoptimizer_FOUND)
    # use separate condition to avoid uninitialized variable warning
    set(orderoptimizer_FOUND FALSE)
  endif()
  return()
endif()
set(_orderoptimizer_CONFIG_INCLUDED TRUE)

# output package information
if(NOT orderoptimizer_FIND_QUIETLY)
  message(STATUS "Found orderoptimizer: 0.1.0 (${orderoptimizer_DIR})")
endif()

# warn when using a deprecated package
if(NOT "" STREQUAL "")
  set(_msg "Package 'orderoptimizer' is deprecated")
  # append custom deprecation text if available
  if(NOT "" STREQUAL "TRUE")
    set(_msg "${_msg} ()")
  endif()
  # optionally quiet the deprecation message
  if(NOT ${orderoptimizer_DEPRECATED_QUIET})
    message(DEPRECATION "${_msg}")
  endif()
endif()

# flag package as ament-based to distinguish it after being find_package()-ed
set(orderoptimizer_FOUND_AMENT_PACKAGE TRUE)

# include all config extra files
set(_extras "")
foreach(_extra ${_extras})
  include("${orderoptimizer_DIR}/${_extra}")
endforeach()
