// generated from rosidl_generator_c/resource/idl.h.em
// with input from orderoptimizer_interfaces:msg/Nextorder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__NEXTORDER_H_
#define ORDEROPTIMIZER_INTERFACES__MSG__NEXTORDER_H_

#include "orderoptimizer_interfaces/msg/detail/nextorder__struct.h"
#include "orderoptimizer_interfaces/msg/detail/nextorder__functions.h"
#include "orderoptimizer_interfaces/msg/detail/nextorder__type_support.h"

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__NEXTORDER_H_
