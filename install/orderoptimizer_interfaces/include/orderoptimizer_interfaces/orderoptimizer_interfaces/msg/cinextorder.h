// generated from rosidl_generator_c/resource/idl.h.em
// with input from orderoptimizer_interfaces:msg/Cinextorder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__CINEXTORDER_H_
#define ORDEROPTIMIZER_INTERFACES__MSG__CINEXTORDER_H_

#include "orderoptimizer_interfaces/msg/detail/cinextorder__struct.h"
#include "orderoptimizer_interfaces/msg/detail/cinextorder__functions.h"
#include "orderoptimizer_interfaces/msg/detail/cinextorder__type_support.h"

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__CINEXTORDER_H_
