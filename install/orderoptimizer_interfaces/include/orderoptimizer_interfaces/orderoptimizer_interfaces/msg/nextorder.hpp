// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__NEXTORDER_HPP_
#define ORDEROPTIMIZER_INTERFACES__MSG__NEXTORDER_HPP_

#include "orderoptimizer_interfaces/msg/detail/nextorder__struct.hpp"
#include "orderoptimizer_interfaces/msg/detail/nextorder__builder.hpp"
#include "orderoptimizer_interfaces/msg/detail/nextorder__traits.hpp"

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__NEXTORDER_HPP_
