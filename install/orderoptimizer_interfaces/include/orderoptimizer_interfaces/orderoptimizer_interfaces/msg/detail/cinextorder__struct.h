// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from orderoptimizer_interfaces:msg/Cinextorder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__CINEXTORDER__STRUCT_H_
#define ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__CINEXTORDER__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'description'
#include "rosidl_runtime_c/string.h"

/// Struct defined in msg/Cinextorder in the package orderoptimizer_interfaces.
typedef struct orderoptimizer_interfaces__msg__Cinextorder
{
  uint32_t order_id;
  rosidl_runtime_c__String description;
} orderoptimizer_interfaces__msg__Cinextorder;

// Struct for a sequence of orderoptimizer_interfaces__msg__Cinextorder.
typedef struct orderoptimizer_interfaces__msg__Cinextorder__Sequence
{
  orderoptimizer_interfaces__msg__Cinextorder * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} orderoptimizer_interfaces__msg__Cinextorder__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__CINEXTORDER__STRUCT_H_
