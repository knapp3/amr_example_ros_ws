// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from orderoptimizer_interfaces:msg/CInextOrder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__STRUCT_HPP_
#define ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__STRUCT_HPP_

#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

#include "rosidl_runtime_cpp/bounded_vector.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


#ifndef _WIN32
# define DEPRECATED__orderoptimizer_interfaces__msg__CInextOrder __attribute__((deprecated))
#else
# define DEPRECATED__orderoptimizer_interfaces__msg__CInextOrder __declspec(deprecated)
#endif

namespace orderoptimizer_interfaces
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct CInextOrder_
{
  using Type = CInextOrder_<ContainerAllocator>;

  explicit CInextOrder_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->order_id = 0ul;
      this->description = "";
    }
  }

  explicit CInextOrder_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  : description(_alloc)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->order_id = 0ul;
      this->description = "";
    }
  }

  // field types and members
  using _order_id_type =
    uint32_t;
  _order_id_type order_id;
  using _description_type =
    std::basic_string<char, std::char_traits<char>, typename std::allocator_traits<ContainerAllocator>::template rebind_alloc<char>>;
  _description_type description;

  // setters for named parameter idiom
  Type & set__order_id(
    const uint32_t & _arg)
  {
    this->order_id = _arg;
    return *this;
  }
  Type & set__description(
    const std::basic_string<char, std::char_traits<char>, typename std::allocator_traits<ContainerAllocator>::template rebind_alloc<char>> & _arg)
  {
    this->description = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator> *;
  using ConstRawPtr =
    const orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__orderoptimizer_interfaces__msg__CInextOrder
    std::shared_ptr<orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__orderoptimizer_interfaces__msg__CInextOrder
    std::shared_ptr<orderoptimizer_interfaces::msg::CInextOrder_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const CInextOrder_ & other) const
  {
    if (this->order_id != other.order_id) {
      return false;
    }
    if (this->description != other.description) {
      return false;
    }
    return true;
  }
  bool operator!=(const CInextOrder_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct CInextOrder_

// alias to use template instance with default allocator
using CInextOrder =
  orderoptimizer_interfaces::msg::CInextOrder_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace orderoptimizer_interfaces

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__STRUCT_HPP_
