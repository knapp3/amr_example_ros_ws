// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from orderoptimizer_interfaces:msg/NextOrder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXT_ORDER__TRAITS_HPP_
#define ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXT_ORDER__TRAITS_HPP_

#include <stdint.h>

#include <sstream>
#include <string>
#include <type_traits>

#include "orderoptimizer_interfaces/msg/detail/next_order__struct.hpp"
#include "rosidl_runtime_cpp/traits.hpp"

namespace orderoptimizer_interfaces
{

namespace msg
{

inline void to_flow_style_yaml(
  const NextOrder & msg,
  std::ostream & out)
{
  out << "{";
  // member: order_id
  {
    out << "order_id: ";
    rosidl_generator_traits::value_to_yaml(msg.order_id, out);
    out << ", ";
  }

  // member: description
  {
    out << "description: ";
    rosidl_generator_traits::value_to_yaml(msg.description, out);
  }
  out << "}";
}  // NOLINT(readability/fn_size)

inline void to_block_style_yaml(
  const NextOrder & msg,
  std::ostream & out, size_t indentation = 0)
{
  // member: order_id
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "order_id: ";
    rosidl_generator_traits::value_to_yaml(msg.order_id, out);
    out << "\n";
  }

  // member: description
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "description: ";
    rosidl_generator_traits::value_to_yaml(msg.description, out);
    out << "\n";
  }
}  // NOLINT(readability/fn_size)

inline std::string to_yaml(const NextOrder & msg, bool use_flow_style = false)
{
  std::ostringstream out;
  if (use_flow_style) {
    to_flow_style_yaml(msg, out);
  } else {
    to_block_style_yaml(msg, out);
  }
  return out.str();
}

}  // namespace msg

}  // namespace orderoptimizer_interfaces

namespace rosidl_generator_traits
{

[[deprecated("use orderoptimizer_interfaces::msg::to_block_style_yaml() instead")]]
inline void to_yaml(
  const orderoptimizer_interfaces::msg::NextOrder & msg,
  std::ostream & out, size_t indentation = 0)
{
  orderoptimizer_interfaces::msg::to_block_style_yaml(msg, out, indentation);
}

[[deprecated("use orderoptimizer_interfaces::msg::to_yaml() instead")]]
inline std::string to_yaml(const orderoptimizer_interfaces::msg::NextOrder & msg)
{
  return orderoptimizer_interfaces::msg::to_yaml(msg);
}

template<>
inline const char * data_type<orderoptimizer_interfaces::msg::NextOrder>()
{
  return "orderoptimizer_interfaces::msg::NextOrder";
}

template<>
inline const char * name<orderoptimizer_interfaces::msg::NextOrder>()
{
  return "orderoptimizer_interfaces/msg/NextOrder";
}

template<>
struct has_fixed_size<orderoptimizer_interfaces::msg::NextOrder>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<orderoptimizer_interfaces::msg::NextOrder>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<orderoptimizer_interfaces::msg::NextOrder>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXT_ORDER__TRAITS_HPP_
