// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from orderoptimizer_interfaces:msg/Nextorder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXTORDER__BUILDER_HPP_
#define ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXTORDER__BUILDER_HPP_

#include <algorithm>
#include <utility>

#include "orderoptimizer_interfaces/msg/detail/nextorder__struct.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


namespace orderoptimizer_interfaces
{

namespace msg
{

namespace builder
{

class Init_Nextorder_description
{
public:
  explicit Init_Nextorder_description(::orderoptimizer_interfaces::msg::Nextorder & msg)
  : msg_(msg)
  {}
  ::orderoptimizer_interfaces::msg::Nextorder description(::orderoptimizer_interfaces::msg::Nextorder::_description_type arg)
  {
    msg_.description = std::move(arg);
    return std::move(msg_);
  }

private:
  ::orderoptimizer_interfaces::msg::Nextorder msg_;
};

class Init_Nextorder_order_id
{
public:
  Init_Nextorder_order_id()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_Nextorder_description order_id(::orderoptimizer_interfaces::msg::Nextorder::_order_id_type arg)
  {
    msg_.order_id = std::move(arg);
    return Init_Nextorder_description(msg_);
  }

private:
  ::orderoptimizer_interfaces::msg::Nextorder msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::orderoptimizer_interfaces::msg::Nextorder>()
{
  return orderoptimizer_interfaces::msg::builder::Init_Nextorder_order_id();
}

}  // namespace orderoptimizer_interfaces

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXTORDER__BUILDER_HPP_
