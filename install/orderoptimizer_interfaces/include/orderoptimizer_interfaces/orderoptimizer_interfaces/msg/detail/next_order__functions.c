// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from orderoptimizer_interfaces:msg/NextOrder.idl
// generated code does not contain a copyright notice
#include "orderoptimizer_interfaces/msg/detail/next_order__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "rcutils/allocator.h"


// Include directives for member types
// Member `description`
#include "rosidl_runtime_c/string_functions.h"

bool
orderoptimizer_interfaces__msg__NextOrder__init(orderoptimizer_interfaces__msg__NextOrder * msg)
{
  if (!msg) {
    return false;
  }
  // order_id
  // description
  if (!rosidl_runtime_c__String__init(&msg->description)) {
    orderoptimizer_interfaces__msg__NextOrder__fini(msg);
    return false;
  }
  return true;
}

void
orderoptimizer_interfaces__msg__NextOrder__fini(orderoptimizer_interfaces__msg__NextOrder * msg)
{
  if (!msg) {
    return;
  }
  // order_id
  // description
  rosidl_runtime_c__String__fini(&msg->description);
}

bool
orderoptimizer_interfaces__msg__NextOrder__are_equal(const orderoptimizer_interfaces__msg__NextOrder * lhs, const orderoptimizer_interfaces__msg__NextOrder * rhs)
{
  if (!lhs || !rhs) {
    return false;
  }
  // order_id
  if (lhs->order_id != rhs->order_id) {
    return false;
  }
  // description
  if (!rosidl_runtime_c__String__are_equal(
      &(lhs->description), &(rhs->description)))
  {
    return false;
  }
  return true;
}

bool
orderoptimizer_interfaces__msg__NextOrder__copy(
  const orderoptimizer_interfaces__msg__NextOrder * input,
  orderoptimizer_interfaces__msg__NextOrder * output)
{
  if (!input || !output) {
    return false;
  }
  // order_id
  output->order_id = input->order_id;
  // description
  if (!rosidl_runtime_c__String__copy(
      &(input->description), &(output->description)))
  {
    return false;
  }
  return true;
}

orderoptimizer_interfaces__msg__NextOrder *
orderoptimizer_interfaces__msg__NextOrder__create()
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  orderoptimizer_interfaces__msg__NextOrder * msg = (orderoptimizer_interfaces__msg__NextOrder *)allocator.allocate(sizeof(orderoptimizer_interfaces__msg__NextOrder), allocator.state);
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(orderoptimizer_interfaces__msg__NextOrder));
  bool success = orderoptimizer_interfaces__msg__NextOrder__init(msg);
  if (!success) {
    allocator.deallocate(msg, allocator.state);
    return NULL;
  }
  return msg;
}

void
orderoptimizer_interfaces__msg__NextOrder__destroy(orderoptimizer_interfaces__msg__NextOrder * msg)
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  if (msg) {
    orderoptimizer_interfaces__msg__NextOrder__fini(msg);
  }
  allocator.deallocate(msg, allocator.state);
}


bool
orderoptimizer_interfaces__msg__NextOrder__Sequence__init(orderoptimizer_interfaces__msg__NextOrder__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  orderoptimizer_interfaces__msg__NextOrder * data = NULL;

  if (size) {
    data = (orderoptimizer_interfaces__msg__NextOrder *)allocator.zero_allocate(size, sizeof(orderoptimizer_interfaces__msg__NextOrder), allocator.state);
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = orderoptimizer_interfaces__msg__NextOrder__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        orderoptimizer_interfaces__msg__NextOrder__fini(&data[i - 1]);
      }
      allocator.deallocate(data, allocator.state);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
orderoptimizer_interfaces__msg__NextOrder__Sequence__fini(orderoptimizer_interfaces__msg__NextOrder__Sequence * array)
{
  if (!array) {
    return;
  }
  rcutils_allocator_t allocator = rcutils_get_default_allocator();

  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      orderoptimizer_interfaces__msg__NextOrder__fini(&array->data[i]);
    }
    allocator.deallocate(array->data, allocator.state);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

orderoptimizer_interfaces__msg__NextOrder__Sequence *
orderoptimizer_interfaces__msg__NextOrder__Sequence__create(size_t size)
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  orderoptimizer_interfaces__msg__NextOrder__Sequence * array = (orderoptimizer_interfaces__msg__NextOrder__Sequence *)allocator.allocate(sizeof(orderoptimizer_interfaces__msg__NextOrder__Sequence), allocator.state);
  if (!array) {
    return NULL;
  }
  bool success = orderoptimizer_interfaces__msg__NextOrder__Sequence__init(array, size);
  if (!success) {
    allocator.deallocate(array, allocator.state);
    return NULL;
  }
  return array;
}

void
orderoptimizer_interfaces__msg__NextOrder__Sequence__destroy(orderoptimizer_interfaces__msg__NextOrder__Sequence * array)
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  if (array) {
    orderoptimizer_interfaces__msg__NextOrder__Sequence__fini(array);
  }
  allocator.deallocate(array, allocator.state);
}

bool
orderoptimizer_interfaces__msg__NextOrder__Sequence__are_equal(const orderoptimizer_interfaces__msg__NextOrder__Sequence * lhs, const orderoptimizer_interfaces__msg__NextOrder__Sequence * rhs)
{
  if (!lhs || !rhs) {
    return false;
  }
  if (lhs->size != rhs->size) {
    return false;
  }
  for (size_t i = 0; i < lhs->size; ++i) {
    if (!orderoptimizer_interfaces__msg__NextOrder__are_equal(&(lhs->data[i]), &(rhs->data[i]))) {
      return false;
    }
  }
  return true;
}

bool
orderoptimizer_interfaces__msg__NextOrder__Sequence__copy(
  const orderoptimizer_interfaces__msg__NextOrder__Sequence * input,
  orderoptimizer_interfaces__msg__NextOrder__Sequence * output)
{
  if (!input || !output) {
    return false;
  }
  if (output->capacity < input->size) {
    const size_t allocation_size =
      input->size * sizeof(orderoptimizer_interfaces__msg__NextOrder);
    rcutils_allocator_t allocator = rcutils_get_default_allocator();
    orderoptimizer_interfaces__msg__NextOrder * data =
      (orderoptimizer_interfaces__msg__NextOrder *)allocator.reallocate(
      output->data, allocation_size, allocator.state);
    if (!data) {
      return false;
    }
    // If reallocation succeeded, memory may or may not have been moved
    // to fulfill the allocation request, invalidating output->data.
    output->data = data;
    for (size_t i = output->capacity; i < input->size; ++i) {
      if (!orderoptimizer_interfaces__msg__NextOrder__init(&output->data[i])) {
        // If initialization of any new item fails, roll back
        // all previously initialized items. Existing items
        // in output are to be left unmodified.
        for (; i-- > output->capacity; ) {
          orderoptimizer_interfaces__msg__NextOrder__fini(&output->data[i]);
        }
        return false;
      }
    }
    output->capacity = input->size;
  }
  output->size = input->size;
  for (size_t i = 0; i < input->size; ++i) {
    if (!orderoptimizer_interfaces__msg__NextOrder__copy(
        &(input->data[i]), &(output->data[i])))
    {
      return false;
    }
  }
  return true;
}
