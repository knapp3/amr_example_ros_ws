// generated from rosidl_typesupport_introspection_c/resource/idl__rosidl_typesupport_introspection_c.h.em
// with input from orderoptimizer_interfaces:msg/Nextorder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXTORDER__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
#define ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXTORDER__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "orderoptimizer_interfaces/msg/rosidl_typesupport_introspection_c__visibility_control.h"

ROSIDL_TYPESUPPORT_INTROSPECTION_C_PUBLIC_orderoptimizer_interfaces
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, orderoptimizer_interfaces, msg, Nextorder)();

#ifdef __cplusplus
}
#endif

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXTORDER__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
