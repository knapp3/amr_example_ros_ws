// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from orderoptimizer_interfaces:msg/CInextOrder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__BUILDER_HPP_
#define ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__BUILDER_HPP_

#include <algorithm>
#include <utility>

#include "orderoptimizer_interfaces/msg/detail/c_inext_order__struct.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


namespace orderoptimizer_interfaces
{

namespace msg
{

namespace builder
{

class Init_CInextOrder_description
{
public:
  explicit Init_CInextOrder_description(::orderoptimizer_interfaces::msg::CInextOrder & msg)
  : msg_(msg)
  {}
  ::orderoptimizer_interfaces::msg::CInextOrder description(::orderoptimizer_interfaces::msg::CInextOrder::_description_type arg)
  {
    msg_.description = std::move(arg);
    return std::move(msg_);
  }

private:
  ::orderoptimizer_interfaces::msg::CInextOrder msg_;
};

class Init_CInextOrder_order_id
{
public:
  Init_CInextOrder_order_id()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_CInextOrder_description order_id(::orderoptimizer_interfaces::msg::CInextOrder::_order_id_type arg)
  {
    msg_.order_id = std::move(arg);
    return Init_CInextOrder_description(msg_);
  }

private:
  ::orderoptimizer_interfaces::msg::CInextOrder msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::orderoptimizer_interfaces::msg::CInextOrder>()
{
  return orderoptimizer_interfaces::msg::builder::Init_CInextOrder_order_id();
}

}  // namespace orderoptimizer_interfaces

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__BUILDER_HPP_
