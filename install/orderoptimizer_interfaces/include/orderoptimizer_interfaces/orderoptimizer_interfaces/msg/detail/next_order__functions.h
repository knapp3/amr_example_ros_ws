// generated from rosidl_generator_c/resource/idl__functions.h.em
// with input from orderoptimizer_interfaces:msg/NextOrder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXT_ORDER__FUNCTIONS_H_
#define ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXT_ORDER__FUNCTIONS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdlib.h>

#include "rosidl_runtime_c/visibility_control.h"
#include "orderoptimizer_interfaces/msg/rosidl_generator_c__visibility_control.h"

#include "orderoptimizer_interfaces/msg/detail/next_order__struct.h"

/// Initialize msg/NextOrder message.
/**
 * If the init function is called twice for the same message without
 * calling fini inbetween previously allocated memory will be leaked.
 * \param[in,out] msg The previously allocated message pointer.
 * Fields without a default value will not be initialized by this function.
 * You might want to call memset(msg, 0, sizeof(
 * orderoptimizer_interfaces__msg__NextOrder
 * )) before or use
 * orderoptimizer_interfaces__msg__NextOrder__create()
 * to allocate and initialize the message.
 * \return true if initialization was successful, otherwise false
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
bool
orderoptimizer_interfaces__msg__NextOrder__init(orderoptimizer_interfaces__msg__NextOrder * msg);

/// Finalize msg/NextOrder message.
/**
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
void
orderoptimizer_interfaces__msg__NextOrder__fini(orderoptimizer_interfaces__msg__NextOrder * msg);

/// Create msg/NextOrder message.
/**
 * It allocates the memory for the message, sets the memory to zero, and
 * calls
 * orderoptimizer_interfaces__msg__NextOrder__init().
 * \return The pointer to the initialized message if successful,
 * otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
orderoptimizer_interfaces__msg__NextOrder *
orderoptimizer_interfaces__msg__NextOrder__create();

/// Destroy msg/NextOrder message.
/**
 * It calls
 * orderoptimizer_interfaces__msg__NextOrder__fini()
 * and frees the memory of the message.
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
void
orderoptimizer_interfaces__msg__NextOrder__destroy(orderoptimizer_interfaces__msg__NextOrder * msg);

/// Check for msg/NextOrder message equality.
/**
 * \param[in] lhs The message on the left hand size of the equality operator.
 * \param[in] rhs The message on the right hand size of the equality operator.
 * \return true if messages are equal, otherwise false.
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
bool
orderoptimizer_interfaces__msg__NextOrder__are_equal(const orderoptimizer_interfaces__msg__NextOrder * lhs, const orderoptimizer_interfaces__msg__NextOrder * rhs);

/// Copy a msg/NextOrder message.
/**
 * This functions performs a deep copy, as opposed to the shallow copy that
 * plain assignment yields.
 *
 * \param[in] input The source message pointer.
 * \param[out] output The target message pointer, which must
 *   have been initialized before calling this function.
 * \return true if successful, or false if either pointer is null
 *   or memory allocation fails.
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
bool
orderoptimizer_interfaces__msg__NextOrder__copy(
  const orderoptimizer_interfaces__msg__NextOrder * input,
  orderoptimizer_interfaces__msg__NextOrder * output);

/// Initialize array of msg/NextOrder messages.
/**
 * It allocates the memory for the number of elements and calls
 * orderoptimizer_interfaces__msg__NextOrder__init()
 * for each element of the array.
 * \param[in,out] array The allocated array pointer.
 * \param[in] size The size / capacity of the array.
 * \return true if initialization was successful, otherwise false
 * If the array pointer is valid and the size is zero it is guaranteed
 # to return true.
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
bool
orderoptimizer_interfaces__msg__NextOrder__Sequence__init(orderoptimizer_interfaces__msg__NextOrder__Sequence * array, size_t size);

/// Finalize array of msg/NextOrder messages.
/**
 * It calls
 * orderoptimizer_interfaces__msg__NextOrder__fini()
 * for each element of the array and frees the memory for the number of
 * elements.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
void
orderoptimizer_interfaces__msg__NextOrder__Sequence__fini(orderoptimizer_interfaces__msg__NextOrder__Sequence * array);

/// Create array of msg/NextOrder messages.
/**
 * It allocates the memory for the array and calls
 * orderoptimizer_interfaces__msg__NextOrder__Sequence__init().
 * \param[in] size The size / capacity of the array.
 * \return The pointer to the initialized array if successful, otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
orderoptimizer_interfaces__msg__NextOrder__Sequence *
orderoptimizer_interfaces__msg__NextOrder__Sequence__create(size_t size);

/// Destroy array of msg/NextOrder messages.
/**
 * It calls
 * orderoptimizer_interfaces__msg__NextOrder__Sequence__fini()
 * on the array,
 * and frees the memory of the array.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
void
orderoptimizer_interfaces__msg__NextOrder__Sequence__destroy(orderoptimizer_interfaces__msg__NextOrder__Sequence * array);

/// Check for msg/NextOrder message array equality.
/**
 * \param[in] lhs The message array on the left hand size of the equality operator.
 * \param[in] rhs The message array on the right hand size of the equality operator.
 * \return true if message arrays are equal in size and content, otherwise false.
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
bool
orderoptimizer_interfaces__msg__NextOrder__Sequence__are_equal(const orderoptimizer_interfaces__msg__NextOrder__Sequence * lhs, const orderoptimizer_interfaces__msg__NextOrder__Sequence * rhs);

/// Copy an array of msg/NextOrder messages.
/**
 * This functions performs a deep copy, as opposed to the shallow copy that
 * plain assignment yields.
 *
 * \param[in] input The source array pointer.
 * \param[out] output The target array pointer, which must
 *   have been initialized before calling this function.
 * \return true if successful, or false if either pointer
 *   is null or memory allocation fails.
 */
ROSIDL_GENERATOR_C_PUBLIC_orderoptimizer_interfaces
bool
orderoptimizer_interfaces__msg__NextOrder__Sequence__copy(
  const orderoptimizer_interfaces__msg__NextOrder__Sequence * input,
  orderoptimizer_interfaces__msg__NextOrder__Sequence * output);

#ifdef __cplusplus
}
#endif

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__NEXT_ORDER__FUNCTIONS_H_
