// generated from rosidl_typesupport_introspection_cpp/resource/idl__type_support.cpp.em
// with input from orderoptimizer_interfaces:msg/Cinextorder.idl
// generated code does not contain a copyright notice

#include "array"
#include "cstddef"
#include "string"
#include "vector"
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_interface/macros.h"
#include "orderoptimizer_interfaces/msg/detail/cinextorder__struct.hpp"
#include "rosidl_typesupport_introspection_cpp/field_types.hpp"
#include "rosidl_typesupport_introspection_cpp/identifier.hpp"
#include "rosidl_typesupport_introspection_cpp/message_introspection.hpp"
#include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

namespace orderoptimizer_interfaces
{

namespace msg
{

namespace rosidl_typesupport_introspection_cpp
{

void Cinextorder_init_function(
  void * message_memory, rosidl_runtime_cpp::MessageInitialization _init)
{
  new (message_memory) orderoptimizer_interfaces::msg::Cinextorder(_init);
}

void Cinextorder_fini_function(void * message_memory)
{
  auto typed_message = static_cast<orderoptimizer_interfaces::msg::Cinextorder *>(message_memory);
  typed_message->~Cinextorder();
}

static const ::rosidl_typesupport_introspection_cpp::MessageMember Cinextorder_message_member_array[2] = {
  {
    "order_id",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_UINT32,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(orderoptimizer_interfaces::msg::Cinextorder, order_id),  // bytes offset in struct
    nullptr,  // default value
    nullptr,  // size() function pointer
    nullptr,  // get_const(index) function pointer
    nullptr,  // get(index) function pointer
    nullptr,  // fetch(index, &value) function pointer
    nullptr,  // assign(index, value) function pointer
    nullptr  // resize(index) function pointer
  },
  {
    "description",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_STRING,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(orderoptimizer_interfaces::msg::Cinextorder, description),  // bytes offset in struct
    nullptr,  // default value
    nullptr,  // size() function pointer
    nullptr,  // get_const(index) function pointer
    nullptr,  // get(index) function pointer
    nullptr,  // fetch(index, &value) function pointer
    nullptr,  // assign(index, value) function pointer
    nullptr  // resize(index) function pointer
  }
};

static const ::rosidl_typesupport_introspection_cpp::MessageMembers Cinextorder_message_members = {
  "orderoptimizer_interfaces::msg",  // message namespace
  "Cinextorder",  // message name
  2,  // number of fields
  sizeof(orderoptimizer_interfaces::msg::Cinextorder),
  Cinextorder_message_member_array,  // message members
  Cinextorder_init_function,  // function to initialize message memory (memory has to be allocated)
  Cinextorder_fini_function  // function to terminate message instance (will not free memory)
};

static const rosidl_message_type_support_t Cinextorder_message_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &Cinextorder_message_members,
  get_message_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace msg

}  // namespace orderoptimizer_interfaces


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
get_message_type_support_handle<orderoptimizer_interfaces::msg::Cinextorder>()
{
  return &::orderoptimizer_interfaces::msg::rosidl_typesupport_introspection_cpp::Cinextorder_message_type_support_handle;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, orderoptimizer_interfaces, msg, Cinextorder)() {
  return &::orderoptimizer_interfaces::msg::rosidl_typesupport_introspection_cpp::Cinextorder_message_type_support_handle;
}

#ifdef __cplusplus
}
#endif
