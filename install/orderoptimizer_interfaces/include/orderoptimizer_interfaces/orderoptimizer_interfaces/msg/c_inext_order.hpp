// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__C_INEXT_ORDER_HPP_
#define ORDEROPTIMIZER_INTERFACES__MSG__C_INEXT_ORDER_HPP_

#include "orderoptimizer_interfaces/msg/detail/c_inext_order__struct.hpp"
#include "orderoptimizer_interfaces/msg/detail/c_inext_order__builder.hpp"
#include "orderoptimizer_interfaces/msg/detail/c_inext_order__traits.hpp"

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__C_INEXT_ORDER_HPP_
