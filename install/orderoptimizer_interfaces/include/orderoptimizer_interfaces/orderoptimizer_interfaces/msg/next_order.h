// generated from rosidl_generator_c/resource/idl.h.em
// with input from orderoptimizer_interfaces:msg/NextOrder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__NEXT_ORDER_H_
#define ORDEROPTIMIZER_INTERFACES__MSG__NEXT_ORDER_H_

#include "orderoptimizer_interfaces/msg/detail/next_order__struct.h"
#include "orderoptimizer_interfaces/msg/detail/next_order__functions.h"
#include "orderoptimizer_interfaces/msg/detail/next_order__type_support.h"

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__NEXT_ORDER_H_
