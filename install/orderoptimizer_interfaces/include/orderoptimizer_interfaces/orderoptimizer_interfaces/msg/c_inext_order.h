// generated from rosidl_generator_c/resource/idl.h.em
// with input from orderoptimizer_interfaces:msg/CInextOrder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__C_INEXT_ORDER_H_
#define ORDEROPTIMIZER_INTERFACES__MSG__C_INEXT_ORDER_H_

#include "orderoptimizer_interfaces/msg/detail/c_inext_order__struct.h"
#include "orderoptimizer_interfaces/msg/detail/c_inext_order__functions.h"
#include "orderoptimizer_interfaces/msg/detail/c_inext_order__type_support.h"

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__C_INEXT_ORDER_H_
