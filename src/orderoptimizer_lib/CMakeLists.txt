cmake_minimum_required(VERSION 3.8)
project(orderoptimizer_lib)

if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# find dependencies
find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(std_msgs REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(yaml-cpp REQUIRED)

if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  # the following line skips the linter which checks for copyrights
  # comment the line when a copyright and license is added to all source files
  set(ament_cmake_copyright_FOUND TRUE)
  # the following line skips cpplint (only works in a git repo)
  # comment the line when this package is in a git repo and when
  # a copyright and license is added to all source files
  set(ament_cmake_cpplint_FOUND TRUE)
  ament_lint_auto_find_test_dependencies()
endif()

# let the compiler search for headers in the include folder
include_directories(include)

# define a library target called orderoptimizer_lib
add_library(orderoptimizer_lib src/orderoptimizer_lib.cpp) 

#Versuch
target_link_libraries(orderoptimizer_lib yaml-cpp)

# this line to exports the library
ament_export_targets(orderoptimizer_lib HAS_LIBRARY_TARGET)

# install the include/orderoptimizer_lib directory to the install/include/orderoptimizer_lib
install(
  DIRECTORY include/orderoptimizer_lib
  DESTINATION include
)

install(
  TARGETS orderoptimizer_lib
  EXPORT orderoptimizer_lib
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  RUNTIME DESTINATION bin
  INCLUDES DESTINATION include
)

ament_package()
