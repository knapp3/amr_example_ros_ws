#include "orderoptimizer_lib/orderoptimizer_lib.h"

#include <iostream>
#include "math.h"

Part::Part(part_t partInfo)
{
    _partInfo = partInfo;
};

part_t Part::getPartInfo()
{
    return this->_partInfo;
};

double Part::getRelDistance(Coords_t currentPos)
{
    return sqrt(fabs(pow(_partInfo.Coords.cx,2)-pow(currentPos.cx,2))+fabs(pow(_partInfo.Coords.cy,2)-pow(currentPos.cy,2)));
}

Product::Product(product_t productInfo)
{
    _productInfo = productInfo;
};

product_t Product::getProductInfo()
{
    return this->_productInfo;
};

std::vector<Part> Product::getParts()
{
    return this->_parts;
}

void Product::setParts(std::vector<Part> parts)
{
    _parts = parts;
}

Products::Products(std::string pathProductList){
    //Parse parts list
    std::cout << "Parsing Products YAML" << std::endl;
    try {
            YAML::Node config = YAML::LoadFile(pathProductList);

            product_t newProductInfo;
            // The outer element is an array
            for(auto dict : config) {
                // The array element contains ID, Product name, parts array containing keys part name, cx, cy
                //id
                newProductInfo.id = dict["id"].as<size_t>();
                //product
                newProductInfo.name = dict["product"].as<std::string>();
                Product newProduct = Product(newProductInfo);
                _ProductsCatalogue.push_back(newProduct);
                //loop over parts array
                auto parts = dict["parts"];
                std::vector<Part> Parts;
                for(auto part : parts) {
                    part_t newPartInfo;
                    newPartInfo.name = part["part"].as<std::string>();
                    newPartInfo.linkedProductID = newProductInfo.id;
                    newPartInfo.Coords.cx = part["cx"].as<double>();
                    newPartInfo.Coords.cy = part["cy"].as<double>();
                    Part newPart = Part(newPartInfo);
                    Parts.push_back(newPart);
                }
                _ProductsCatalogue.back().setParts(Parts);
                }

    } catch(const YAML::BadFile& e) {
        std::cerr << e.msg << std::endl;
    } catch(const YAML::ParserException& e) {
        std::cerr << e.msg << std::endl;
    } catch(const YAML::BadConversion& e) {
        std::cout << e.msg << std::endl;
    }
};

void Products::printProductsList()
{
    std::cout << "Print products" << std::endl;

    for (auto &product : _ProductsCatalogue)
    {
        std::cout << "ProductID: " << product.getProductInfo().id << std::endl;
        std::cout << "Productname: " << product.getProductInfo().name << std::endl;
        for (auto &part : product.getParts())
        {
            std::cout << "    Part: " << part.getPartInfo().name << std::endl;
            std::cout << "        cx: " << part.getPartInfo().Coords.cx << std::endl;
            std::cout << "        cy: " << part.getPartInfo().Coords.cy << std::endl;
            std::cout << "        RelProd: " << part.getPartInfo().linkedProductID << std::endl;
        }
    }
};

Product Products::getProdbyID(size_t id)
{
    for (auto &product : _ProductsCatalogue)
    {
        if (product.getProductInfo().id == id)
        {
            return product;
        }
    }

    //throw std::invalid_argument( "Error: Invalid Product ID" );
    product_t nullProd;
    Product nullProduct(nullProd);
    std::cout << "Error Product ID not found." << std::endl;
    return nullProduct;
};

Orders::Orders(std::string pathOrderList, Products* pavailableProducts){
    //Parse parts list
    std::cout << "Parsing Orders YAML" << std::endl;
    try {
        YAML::Node orders = YAML::LoadFile(pathOrderList);

        intorder_t newOrder;

        for(auto dict : orders) {
            // The array element contains ID, PutCoordinates, Products array
            //id
            newOrder.setID(dict["order"].as<size_t>());
            //Coords PutPlace
            newOrder.putPlace.cx = dict["cx"].as<double>();
            newOrder.putPlace.cy = dict["cy"].as<double>();
            _OrderList.push_back(newOrder);
            //loop over products array
            auto products = dict["products"];
            std::vector<Product> Products;
            for(auto productid : products) {
                Product newProduct = pavailableProducts->getProdbyID(productid.as<size_t>());
                Products.push_back(newProduct);
            }
            _OrderList.back().setProducts(Products);
        }
    } catch(const YAML::BadFile& e) {
        std::cerr << e.msg << std::endl;
    } catch(const YAML::ParserException& e) {
        std::cerr << e.msg << std::endl;
    }
};

void Orders::printOrderList()
{
    std::cout << "Print orders" << std::endl;

    for (auto &order : _OrderList)
    {
        std::cout << "OrderID: " << order.orderInfo()->getID() << std::endl;
        std::cout << "    Order PutPos: " << std::endl;
        std::cout << "        cx: " << order.orderInfo()->putPlace.cx << std::endl;
        std::cout << "        cy: " << order.orderInfo()->putPlace.cy << std::endl;
        for (auto &product : order.getProducts())
        {
            std::cout << "    ProductID: " << product.getProductInfo().id << std::endl;
            std::cout << "    Productname: " << product.getProductInfo().name << std::endl;
            for (auto &part : product.getParts())
        {
            std::cout << "    Part: " << part.getPartInfo().name << std::endl;
            std::cout << "        cx: " << part.getPartInfo().Coords.cx << std::endl;
            std::cout << "        cy: " << part.getPartInfo().Coords.cy << std::endl;
            std::cout << "        RelProd: " << part.getPartInfo().linkedProductID << std::endl;
        }
        }
    }
};

Order Orders::getOrderbyID(size_t id)
{
    for (auto &order : _OrderList)
    {
        if (order.orderInfo()->getID() == id)
        {
            return order;
        }
    }
    
    //throw std::invalid_argument( "Error: Invalid Order ID" );
    std::cout << "Error: Order ID not found." << std::endl;
    intorder_t nullOrd;
    Order nullOrder = Order(nullOrd);
    return nullOrder;
    
};

Order::Order(intorder_t orderInfo)
{
    _orderInfo = orderInfo;
};

intorder_t* Order::orderInfo()
{
    return this->_orderInfo.orderInfo();
};

std::vector<Product> Order::getProducts()
{
    return this->_products;
};

void Order::setProducts(std::vector<Product> products)
{
    _products = products;
};