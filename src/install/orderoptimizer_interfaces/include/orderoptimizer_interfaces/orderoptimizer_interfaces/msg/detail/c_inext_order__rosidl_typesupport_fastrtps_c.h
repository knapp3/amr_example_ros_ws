// generated from rosidl_typesupport_fastrtps_c/resource/idl__rosidl_typesupport_fastrtps_c.h.em
// with input from orderoptimizer_interfaces:msg/CInextOrder.idl
// generated code does not contain a copyright notice
#ifndef ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
#define ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_


#include <stddef.h>
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "orderoptimizer_interfaces/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_orderoptimizer_interfaces
size_t get_serialized_size_orderoptimizer_interfaces__msg__CInextOrder(
  const void * untyped_ros_message,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_orderoptimizer_interfaces
size_t max_serialized_size_orderoptimizer_interfaces__msg__CInextOrder(
  bool & full_bounded,
  bool & is_plain,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_orderoptimizer_interfaces
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, orderoptimizer_interfaces, msg, CInextOrder)();

#ifdef __cplusplus
}
#endif

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
