// generated from rosidl_typesupport_fastrtps_cpp/resource/idl__rosidl_typesupport_fastrtps_cpp.hpp.em
// with input from orderoptimizer_interfaces:msg/CInextOrder.idl
// generated code does not contain a copyright notice

#ifndef ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__ROSIDL_TYPESUPPORT_FASTRTPS_CPP_HPP_
#define ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__ROSIDL_TYPESUPPORT_FASTRTPS_CPP_HPP_

#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "orderoptimizer_interfaces/msg/rosidl_typesupport_fastrtps_cpp__visibility_control.h"
#include "orderoptimizer_interfaces/msg/detail/c_inext_order__struct.hpp"

#ifndef _WIN32
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wunused-parameter"
# ifdef __clang__
#  pragma clang diagnostic ignored "-Wdeprecated-register"
#  pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
# endif
#endif
#ifndef _WIN32
# pragma GCC diagnostic pop
#endif

#include "fastcdr/Cdr.h"

namespace orderoptimizer_interfaces
{

namespace msg
{

namespace typesupport_fastrtps_cpp
{

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_orderoptimizer_interfaces
cdr_serialize(
  const orderoptimizer_interfaces::msg::CInextOrder & ros_message,
  eprosima::fastcdr::Cdr & cdr);

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_orderoptimizer_interfaces
cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  orderoptimizer_interfaces::msg::CInextOrder & ros_message);

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_orderoptimizer_interfaces
get_serialized_size(
  const orderoptimizer_interfaces::msg::CInextOrder & ros_message,
  size_t current_alignment);

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_orderoptimizer_interfaces
max_serialized_size_CInextOrder(
  bool & full_bounded,
  bool & is_plain,
  size_t current_alignment);

}  // namespace typesupport_fastrtps_cpp

}  // namespace msg

}  // namespace orderoptimizer_interfaces

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_orderoptimizer_interfaces
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, orderoptimizer_interfaces, msg, CInextOrder)();

#ifdef __cplusplus
}
#endif

#endif  // ORDEROPTIMIZER_INTERFACES__MSG__DETAIL__C_INEXT_ORDER__ROSIDL_TYPESUPPORT_FASTRTPS_CPP_HPP_
