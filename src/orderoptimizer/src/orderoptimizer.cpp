#include <filesystem>
#include <chrono>
#include <future>
#include <fstream> 

#include "orderoptimizer_lib/orderoptimizer_lib.h"

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "visualization_msgs/msg/marker_array.hpp"
#include "orderoptimizer_interfaces/msg/nextorder.hpp"

using namespace std::placeholders;

class optimizer : public rclcpp::Node
{
    public:
        optimizer() : Node("OrderOptimizer")
        {
            //Declare parameter
            this->declare_parameter("configFolder_absPath", "config/smallSet");

            RCLCPP_INFO(get_logger(), "Change folder parameter by ros2 param set /OrderOptimizer configFolder_absPath <absolutePathToFolder>");
            
            // Create a parameter change subscriber
            param_subscriber_ = std::make_shared<rclcpp::ParameterEventHandler>(this);
            
            //Set Callback
            auto paramChangeCallback = [this](const rclcpp::Parameter & param)
            {
                std::string parameter = param.as_string();
                RCLCPP_INFO(get_logger(), "Received parameter change, trying to set config path to %s", parameter.c_str());
                                
                if (std::filesystem::is_directory(parameter))
                {
                    std::string parseFile = parameter + "/configuration/products.yaml";
                    //Parse configuration/products
                    RCLCPP_INFO(get_logger(), "Parsing products from file: %s", parseFile.c_str());
                    //Parse products YAML
                    productsCatalogue = new Products(parseFile);

                    RCLCPP_INFO(get_logger(), "Parsing products finished. Waiting for msgs on topics /currentPosition and /nextOrder ...");
                }
                else
                {
                    RCLCPP_ERROR(get_logger(), "%s is not a valid directory", parameter.c_str());
                }
                
            };

            //Create paramChangeCallback
            paramChange_handle = this->param_subscriber_->add_parameter_callback("configFolder_absPath", paramChangeCallback);

            // Create currentPosition subscriber
            currentPositionSub = create_subscription<geometry_msgs::msg::PoseStamped>("currentPosition", 10,
                    std::bind(&optimizer::currentPosCallback, this, _1) );
            
            //Create nextOrder subscriber
            nextOrderSub = create_subscription<orderoptimizer_interfaces::msg::Nextorder>("nextOrder", 10,
                    std::bind(&optimizer::nextOrderCallback, this, _1) );
            

            
            markerArrayPub = this->create_publisher<visualization_msgs::msg::MarkerArray>("markerArray", 10); 
        }

    protected:
        void currentPosCallback(const geometry_msgs::msg::PoseStamped::SharedPtr msg)
        {
            //msg -> currentPos
            currentPos.cx = msg->pose.position.x;
            currentPos.cy = msg->pose.position.y;
            //Log
            RCLCPP_INFO(get_logger(), "Received currentPos in '%s' frame : X: %.2f Y: %.2f - Timestamp: %u.%u sec ",
                msg->header.frame_id.c_str(),
                currentPos.cx, currentPos.cy,
                msg->header.stamp.sec,msg->header.stamp.nanosec);
        }

        void nextOrderCallback(const orderoptimizer_interfaces::msg::Nextorder::SharedPtr msg)
        {
            currentOrder.setID(msg->order_id);
            currentOrder.description = msg->description;
            //Log
            RCLCPP_INFO(get_logger(), "Received nextOrder with id: %lu and description: %s",
                currentOrder.getID(), currentOrder.description.c_str());
            //Find correct order file
            std::string orderFile = findOrderFile(currentOrder.getID(), this->get_parameter("configFolder_absPath").as_string() + "/orders/");
            if (orderFile.length() > 0)
            {
                // Successful - parse order, do opt path, do visu
                //Empty MarkerArray
                Markers.markers.clear();
                RCLCPP_INFO(get_logger(), "Parsing order file: %s", orderFile.c_str());
                //Parse orders/orders
                orderList = new Orders(orderFile, productsCatalogue);
                optPathPartList = calcOptPath(getCurrOrder());
                printPath();
                
            }
            else
            {
                // Error - stop here
                RCLCPP_ERROR(get_logger(), "File not found for orderID: %lu", currentOrder.getID());
            }
            }

    private:
        rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr currentPositionSub;
        rclcpp::Subscription<orderoptimizer_interfaces::msg::Nextorder>::SharedPtr nextOrderSub;
        rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr markerArrayPub;
        std::shared_ptr<rclcpp::ParameterEventHandler> param_subscriber_;
        std::shared_ptr<rclcpp::ParameterCallbackHandle> paramChange_handle;

        Products *productsCatalogue;
        Orders *orderList;

        extorder_t currentOrder;
        Coords_t currentPos;

        //Visualization
        visualization_msgs::msg::MarkerArray Markers;


        Order getCurrOrder()
        {
            return orderList->getOrderbyID(currentOrder.getID());
        }

        struct relDistComparator 
        {
            explicit relDistComparator(Coords_t currentPos_) : currentPos(currentPos_) {}

            bool operator ()(Part p1, Part p2) const 
            {
                return (p1.getRelDistance(currentPos) < p2.getRelDistance(currentPos));
            }
            Coords_t currentPos;
        };

        std::vector<Part> calcOptPath(Order order)
        {
            //Get complete partslist for order
            std::vector<Part> complPartsList;
            for (auto &product : order.getProducts())
            {
                for (auto &part : product.getParts())
                {
                complPartsList.push_back(part);
                }
            }
            
            //Sort ascending by relative distance to currentPos
            sort(complPartsList.begin(), complPartsList.end(), relDistComparator(currentPos)); 

            return complPartsList;
        }

        std::vector<Part> optPathPartList;
        
        void printPath()
        {
            size_t idx = 0;
            //Visu Marker currentPos
            visualization_msgs::msg::Marker marker;
            marker.header.frame_id = "map";
            marker.header.stamp = rclcpp::Time();
            marker.ns = "orderoptimizer";
            marker.id = idx;
            marker.type = visualization_msgs::msg::Marker::CUBE;
            marker.action = visualization_msgs::msg::Marker::ADD;
            marker.pose.position.x = currentPos.cx;
            marker.pose.position.y = currentPos.cy;
            marker.pose.position.z = 0;
            marker.pose.orientation.x = 0.0;
            marker.pose.orientation.y = 0.0;
            marker.pose.orientation.z = 0.0;
            marker.pose.orientation.w = 1.0;
            marker.scale.x = 10;
            marker.scale.y = 10;
            marker.scale.z = 10;
            marker.color.a = 1.0;
            marker.color.r = 0.0;
            marker.color.g = 1.0;
            marker.color.b = 0.0;
            Markers.markers.push_back(marker);
            idx++;
            std::cout << "Working on order " << currentOrder.getID() << " (" << currentOrder.description << ")" << std::endl;
            
            for (auto &part : optPathPartList)
            {
              std::cout << idx << ". "
                        << "Fetching part ‘" << part.getPartInfo().name 
                        << "‘ for product ‘" << part.getPartInfo().linkedProductID
                        << "‘ at x: "        << part.getPartInfo().Coords.cx
                        << ", y: "           << part.getPartInfo().Coords.cy << std::endl;
            //Visu Marker pickUp
            marker.header.stamp = rclcpp::Time();
            marker.id = idx;
            marker.type = visualization_msgs::msg::Marker::CYLINDER;
            marker.pose.position.x = part.getPartInfo().Coords.cx;
            marker.pose.position.y = part.getPartInfo().Coords.cy;
            marker.color.g = 0.0;
            marker.color.b = 1.0;
            Markers.markers.push_back(marker);
            //Artificial sleep 500ms
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            idx++;
            }
            std::cout   << idx << ". "
                        << "Delivering to destination "
                        << "x: "    << getCurrOrder().orderInfo()->putPlace.cx
                        << ", y: "  << getCurrOrder().orderInfo()->putPlace.cy << std::endl;
            //Visu Marker putPlace
            marker.header.stamp = rclcpp::Time();
            marker.id = idx;
            marker.type = visualization_msgs::msg::Marker::CUBE;
            marker.pose.position.x = getCurrOrder().orderInfo()->putPlace.cx;
            marker.pose.position.y = getCurrOrder().orderInfo()->putPlace.cy;
            marker.color.r = 1.0;
            marker.color.g = 0.0;
            marker.color.b = 0.0;
            Markers.markers.push_back(marker);


            //Publish MarkerArray
            markerArrayPub.get()->publish(Markers);
            RCLCPP_INFO(this->get_logger(), "Published Marker array msg on topic: '/markerArray'");
        }

        struct orderFileInfo_t
        {
            size_t orderid;
            std::filesystem::path path;
        };

        //Function used for parallel parsing
        static std::vector<orderFileInfo_t> parsefilefunc(std::filesystem::path filepath)
        {
            std::vector<orderFileInfo_t> fileInfos;
            
            //Drop YAML-CPP parsing in favor of speed
            std::ifstream file(filepath); 
            std::string line; 
            std::vector<size_t> orderids;
            if (file.is_open()) { 
                std::string orderMark = "- order: ";
                while (getline(file, line)) { 
                    if (line.find(orderMark) != std::string::npos)
                    {
                        std::stringstream sstream(line.substr(orderMark.size()));
                        size_t orderid;
                        sstream >> orderid;
                        orderFileInfo_t fileInfo;
                        fileInfo.orderid = orderid;
                        fileInfo.path = filepath;
                        fileInfos.push_back(fileInfo);
                    }
                } 
            }

            file.close();
            for (auto& orderid : orderids)
            {
                std::cout << orderid << std::endl;
            }
               
            return fileInfos;
        }
        
        std::string findOrderFile(size_t orderID, std::string path)
        {
            std::vector<std::future<std::vector<orderFileInfo_t>>> futures;
            std::vector<orderFileInfo_t> parsedInfo;
            if (std::filesystem::is_directory(path))
            {
                for (const auto& file : std::filesystem::directory_iterator(path))
                {
                    //Start async ops for valid files only
                    if (file.path().extension() == ".yaml")
                    {
                        std::cout << "Parsing content of yaml file: " << file.path().c_str() << std::endl;
                        futures.push_back(std::async(std::launch::async, parsefilefunc, file.path()));
                    }   
                }
                
                for (auto& future : futures)
                {
                    //Merge results of the async ops
                    auto result = future.get();
                    std::copy(result.begin(), result.end(), std::back_inserter(parsedInfo));
                }                

                std::cout << "Searching file containing orderID: "<< orderID << std::endl;
                for (auto& fileinfo : parsedInfo)
                {
                    if (fileinfo.orderid == orderID)
                    {
                        std::cout << "Successful - Found orderID: " << orderID << " in file: " << fileinfo.path.c_str() << std::endl;
                        return fileinfo.path;

                    }
                }
            }
            else
            {
                RCLCPP_ERROR(get_logger(), "%s is not a valid directory", path.c_str());
                return "";
            }
            
            std::cout << "Order file not found." << std::endl;
            return "";
        }
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<optimizer>());
  rclcpp::shutdown();

  return 0;
}
