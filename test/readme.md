# ROS2 package amr_example_ros_ws test cases

Test cases for manual testing of unit. For each test case run the two shell scripts (shell1.sh and shell2.sh) in two separate terminals respectively.

## Test0

Test description:

 - successful order
 - arbitrary currentPosition
 - valid order_id
 - valid config path
 
Expected output on terminal1 (node terminal):

    [INFO] [1715239335.434697803] [OrderOptimizer]: Change folder parameter by ros2 param set /OrderOptimizer configFolder_absPath <absolutePathToFolder>
    [INFO] [1715239340.799271917] [OrderOptimizer]: Received parameter change, trying to set config path to /root/amr_example_ros_ws/config/
    [INFO] [1715239340.799352732] [OrderOptimizer]: Parsing products from file: /root/amr_example_ros_ws/config//configuration/products.yaml
    Parsing Products YAML
    [INFO] [1715239340.881461086] [OrderOptimizer]: Parsing products finished. Waiting for msgs on topics /currentPosition and /nextOrder ...
    [INFO] [1715239344.913658874] [OrderOptimizer]: Received currentPos in 'someID' frame : X: 10.50 Y: 11.20 - Timestamp: 2.100 sec
    [INFO] [1715239347.914956135] [OrderOptimizer]: Received nextOrder with id: 1200007 and description: Some Description
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201205.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201203.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201202.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201204.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201201.yaml
    Searching file containing orderID: 1200007
    Successful - Found orderID: 1200007 in file: /root/amr_example_ros_ws/config//orders/orders_20201203.yaml
    [INFO] [1715239348.848013025] [OrderOptimizer]: Parsing order file: /root/amr_example_ros_ws/config//orders/orders_20201203.yaml
    Parsing Orders YAML
    Working on order 1200007 (Some Description)
    1. Fetching part ‘Part C‘ for product ‘885‘ at x: 281.394, y: 68.3963
    2. Fetching part ‘Part C‘ for product ‘962‘ at x: 281.394, y: 68.3963
    3. Fetching part ‘Part C‘ for product ‘962‘ at x: 281.394, y: 68.3963
    4. Fetching part ‘Part C‘ for product ‘80‘ at x: 281.394, y: 68.3963
    5. Fetching part ‘Part B‘ for product ‘80‘ at x: 550.099, y: 655.423
    6. Fetching part ‘Part B‘ for product ‘80‘ at x: 550.099, y: 655.423
    7. Fetching part ‘Part A‘ for product ‘885‘ at x: 791.863, y: 732.232
    8. Fetching part ‘Part A‘ for product ‘885‘ at x: 791.863, y: 732.232
    9. Fetching part ‘Part A‘ for product ‘962‘ at x: 791.863, y: 732.232
    10. Fetching part ‘Part A‘ for product ‘962‘ at x: 791.863, y: 732.232
    11. Delivering to destination x: 51.8041, y: 765.369
    [INFO] [1715239359.430490631] [OrderOptimizer]: Published Marker array msg on topic: '/markerArray'

## Test1

Test description:

 - successful order
 - currentPosition = Part B coordinates
 - valid order_id
 - valid config path

Expected output on terminal1 (node terminal):

    [INFO] [1715239394.154815320] [OrderOptimizer]: Change folder parameter by ros2 param set /OrderOptimizer configFolder_absPath <absolutePathToFolder>
    [INFO] [1715239401.081415183] [OrderOptimizer]: Received parameter change, trying to set config path to /root/amr_example_ros_ws/config/
    [INFO] [1715239401.082290335] [OrderOptimizer]: Parsing products from file: /root/amr_example_ros_ws/config//configuration/products.yaml
    Parsing Products YAML
    [INFO] [1715239401.162749786] [OrderOptimizer]: Parsing products finished. Waiting for msgs on topics /currentPosition and /nextOrder ...
    [INFO] [1715239404.013013475] [OrderOptimizer]: Received currentPos in 'someID' frame : X: 550.10 Y: 655.42 - Timestamp: 2.100 sec
    [INFO] [1715239406.975691761] [OrderOptimizer]: Received nextOrder with id: 1200007 and description: Some Description
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201205.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201203.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201202.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201204.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201201.yaml
    Searching file containing orderID: 1200007
    Successful - Found orderID: 1200007 in file: /root/amr_example_ros_ws/config//orders/orders_20201203.yaml
    [INFO] [1715239407.905215005] [OrderOptimizer]: Parsing order file: /root/amr_example_ros_ws/config//orders/orders_20201203.yaml
    Parsing Orders YAML
    Working on order 1200007 (Some Description)
    1. Fetching part ‘Part B‘ for product ‘80‘ at x: 550.099, y: 655.423
    2. Fetching part ‘Part B‘ for product ‘80‘ at x: 550.099, y: 655.423
    3. Fetching part ‘Part A‘ for product ‘885‘ at x: 791.863, y: 732.232
    4. Fetching part ‘Part A‘ for product ‘885‘ at x: 791.863, y: 732.232
    5. Fetching part ‘Part A‘ for product ‘962‘ at x: 791.863, y: 732.232
    6. Fetching part ‘Part A‘ for product ‘962‘ at x: 791.863, y: 732.232
    7. Fetching part ‘Part C‘ for product ‘885‘ at x: 281.394, y: 68.3963
    8. Fetching part ‘Part C‘ for product ‘962‘ at x: 281.394, y: 68.3963
    9. Fetching part ‘Part C‘ for product ‘962‘ at x: 281.394, y: 68.3963
    10. Fetching part ‘Part C‘ for product ‘80‘ at x: 281.394, y: 68.3963
    11. Delivering to destination x: 51.8041, y: 765.369
    [INFO] [1715239418.266918118] [OrderOptimizer]: Published Marker array msg on topic: '/markerArray'
    
## Test2

Test description:

 - successful order
 - arbitrary currentPosition
 - invalid order_id
 - valid config path

Expected output on terminal1 (node terminal):

    [INFO] [1715239451.249255144] [OrderOptimizer]: Change folder parameter by ros2 param set /OrderOptimizer configFolder_absPath <absolutePathToFolder>
    [INFO] [1715239462.903299767] [OrderOptimizer]: Received parameter change, trying to set config path to /root/amr_example_ros_ws/config/
    [INFO] [1715239462.903509450] [OrderOptimizer]: Parsing products from file: /root/amr_example_ros_ws/config//configuration/products.yaml
    Parsing Products YAML
    [INFO] [1715239462.984147080] [OrderOptimizer]: Parsing products finished. Waiting for msgs on topics /currentPosition and /nextOrder ...
    [INFO] [1715239466.841635025] [OrderOptimizer]: Received currentPos in 'someID' frame : X: 10.50 Y: 11.20 - Timestamp: 2.100 sec
    [INFO] [1715239469.903322834] [OrderOptimizer]: Received nextOrder with id: 1 and description: Some Description
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201205.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201203.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201202.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201204.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201201.yaml
    Searching file containing orderID: 1
    Order file not found.
    [ERROR] [1715239470.742577676] [OrderOptimizer]: File not found for orderID: 1
    

## Test3

Test description:

 - successful order
 - arbitrary currentPosition
 - valid order_id
 - invalid config path
 
Expected output on terminal1 (node terminal):

    [INFO] [1715239498.967682280] [OrderOptimizer]: Change folder parameter by ros2 param set /OrderOptimizer configFolder_absPath <absolutePathToFolder>
    [INFO] [1715239505.532405556] [OrderOptimizer]: Received parameter change, trying to set config path to /root/amr_example_ros_ws/config/nonexistentPath/
    [ERROR] [1715239505.535321287] [OrderOptimizer]: /root/amr_example_ros_ws/config/nonexistentPath/ is not a valid directory
    [INFO] [1715239508.631566099] [OrderOptimizer]: Received currentPos in 'someID' frame : X: 10.50 Y: 11.20 - Timestamp: 2.100 sec
    [INFO] [1715239511.628483512] [OrderOptimizer]: Received nextOrder with id: 1200007 and description: Some Description
    [ERROR] [1715239511.628538085] [OrderOptimizer]: /root/amr_example_ros_ws/config/nonexistentPath//orders/ is not a valid directory
    [ERROR] [1715239511.628548315] [OrderOptimizer]: File not found for orderID: 1200007
    