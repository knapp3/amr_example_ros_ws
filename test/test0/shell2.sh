#!/bin/bash
source /opt/ros/humble/setup.bash

configDir="${HOME}/amr_example_ros_ws/config/"

cd ${HOME}/amr_example_ros_ws/
source install/setup.bash
echo "Set config directory to " ${configDir} "."
ros2 param set /OrderOptimizer configFolder_absPath ${configDir}
sleep 2
echo "Set arbitrary current position"
ros2 topic pub --once /currentPosition geometry_msgs/msg/PoseStamped "{header:{stamp: {sec: 2, nanosec: 100}, frame_id: 'someID'}, pose: {position: {x: 10.5, y: 11.2, z: 0}, orientation: {x: 0.0, y: 0.0, z: 0.0, w: 1.0}}}"
sleep 2
echo "Set valid order ID"
ros2 topic pub --once /nextOrder orderoptimizer_interfaces/msg/Nextorder "{order_id: 1200007, description: 'Some Description'}"