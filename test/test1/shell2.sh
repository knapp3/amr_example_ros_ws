#!/bin/bash
source /opt/ros/humble/setup.bash

configDir="${HOME}/amr_example_ros_ws/config/"

cd ${HOME}/amr_example_ros_ws/
source install/setup.bash
echo "Set config directory to " ${configDir} "."
ros2 param set /OrderOptimizer configFolder_absPath ${configDir}
sleep 2
echo "Set current position to part B coordinates (x: 550.099, y: 655.423)"
ros2 topic pub --once /currentPosition geometry_msgs/msg/PoseStamped "{header:{stamp: {sec: 2, nanosec: 100}, frame_id: 'someID'}, pose: {position: {x: 550.099, y: 655.423, z: 0}, orientation: {x: 0.0, y: 0.0, z: 0.0, w: 1.0}}}"
sleep 2
echo "Set valid order ID"
ros2 topic pub --once /nextOrder orderoptimizer_interfaces/msg/Nextorder "{order_id: 1200007, description: 'Some Description'}"