    [INFO] [1715239451.249255144] [OrderOptimizer]: Change folder parameter by ros2 param set /OrderOptimizer configFolder_absPath <absolutePathToFolder>
    [INFO] [1715239462.903299767] [OrderOptimizer]: Received parameter change, trying to set config path to /root/amr_example_ros_ws/config/
    [INFO] [1715239462.903509450] [OrderOptimizer]: Parsing products from file: /root/amr_example_ros_ws/config//configuration/products.yaml
    Parsing Products YAML
    [INFO] [1715239462.984147080] [OrderOptimizer]: Parsing products finished. Waiting for msgs on topics /currentPosition and /nextOrder ...
    [INFO] [1715239466.841635025] [OrderOptimizer]: Received currentPos in 'someID' frame : X: 10.50 Y: 11.20 - Timestamp: 2.100 sec
    [INFO] [1715239469.903322834] [OrderOptimizer]: Received nextOrder with id: 1 and description: Some Description
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201205.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201203.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201202.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201204.yaml
    Parsing content of yaml file: /root/amr_example_ros_ws/config//orders/orders_20201201.yaml
    Searching file containing orderID: 1
    Order file not found.
    [ERROR] [1715239470.742577676] [OrderOptimizer]: File not found for orderID: 1
    