# ROS2 package amr_example_ROS_ws/orderoptimizer

ROS2 package amr_example_ROS_ws implementation

## Dependencies
This implementation relies on [YAML C++](https://github.com/jbeder/yaml-cpp) library for yaml file import.

## Prerequisites
Install ROS2 "humble" as described in [ROS 2](https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html) install guide.
Install all listed ROS2 packages:

    sudo  apt  install  ros-humble-desktop
    sudo  apt  install  ros-humble-ros-base
    sudo  apt  install  ros-dev-tools

Install [rosdep](https://docs.ros.org/en/humble/Tutorials/Intermediate/Rosdep.html)

    sudo apt install python3-rosdep

## How to use

    cd ${HOME}
    git clone https://gitlab.com/knapp3/amr_example_ros_ws.git

Point all hard links of all scripts within `/home/<user>/amr_example_ROS_ws/install` (currently pointing to `/root/...`) to your current `<user>`.

Within the workspace directory `/home/<user>/amr_example_ROS_ws` do a

    sudo rosdep init
    rosdep update
    rosdep install --from-paths src --ignore-src -r -y --rosdistro humble

Then build the project

    source /opt/ros/humble/setup.bash
    source install/setup.bash
    colcon build

## Tests
Follow instructions found in `test/readme.md`